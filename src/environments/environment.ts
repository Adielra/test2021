// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyCPnQtMKapPD6IEquPpoTGTBMl6zHzeVA4",
    authDomain: "test2021-9bb24.firebaseapp.com",
    projectId: "test2021-9bb24",
    storageBucket: "test2021-9bb24.appspot.com",
    messagingSenderId: "138422939682",
    appId: "1:138422939682:web:6b005199949d8dff0f5a7b"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
