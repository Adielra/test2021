export interface Student {
    GPT: number;
    SAT: number;
    Tuition: boolean;
    id?:string;
    saved?:Boolean;
    result?:string;
}

