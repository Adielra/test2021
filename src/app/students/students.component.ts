import { StudentService } from './../students.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Student } from '../interfaces/student';
import { StudentService} from '../students.service';


@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  userId;

  students:Student[];
  students$;
  addStudentFormOpen;
  rowToEdit:number = -1; 
  studentToEdit:Student = {GPT:null, SAT:null, Tuition:null};

  add(student:Student){
    this.studentsService.addStudent(this.userId, student.GPT, student.SAT)
  }
  
  moveToEditState(index){
    console.log(this.students[index].GPT);
    this.studentToEdit.GPT = this.students[index].GPT;
    this.studentToEdit.SAT = this.students[index].SAT;
    this.studentToEdit.Tuition = this.students[index].Tuition;
    this.rowToEdit = index; 
  }

  updateStudent(){
    let id = this.students[this.rowToEdit].id;
    this.studentsService.updateStudent(this.userId,id, this.studentToEdit.GPT,this.studentToEdit.SAT,this.studentToEdit.Tuition);
    this.rowToEdit = null;
  }

  deleteStudent(index){
    let id = this.students[index].id;
    this.studentsService.deleteStudent(this.userId, id);
  }

  updateResult(index){
    this.students[index].saved = true; 
    this.studentsService.updateRsult(this.userId,this.students[index].id,this.students[index].result);
  }

  predict(index){
    this.students[index].result = 'Will difault';
    this.predictionService.predict(this.students[index].GPT, this.students[index].SAT, this.students[index].Tuition).subscribe(
      res => {console.log(res);
        if(res > 0.5){
          var result = 'Will pay';
        } else {
          var result = 'Will default'
        }
        this.students[index].result = result}
    );  
  }

  displayedColumns: string[] = ['name', 'Education in years', 'Personal income','Delete', 'Edit', 'Predict', 'Result'];
 
  constructor(private StudentService:StudentService,
    private authService:AuthService,
    private predictionService:PredictService ) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
          this.userId = user.uid;
          console.log(user.uid);
          this.students$ = this.studentsService.getStudents(this.userId);
          this.students$.subscribe(
            docs => {         
              this.students = [];
              var i = 0;
              for (let document of docs) {
                console.log(i++); 
                const student:Student = document.payload.doc.data();
                if(student.result){
                  student.saved = true; 
                }
                student.id = document.payload.doc.id;
                   this.students.push(student); 
              }                        
            }
          )
      })
  }   
}
