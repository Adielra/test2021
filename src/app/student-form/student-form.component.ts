import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Student } from '../interfaces/student';
import { PredictService } from '../predict.service';
import { StudentService } from '../students.service';

@Component({
  selector: 'studentform',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css']
})
export class StudentFormComponent implements OnInit {
  SAT:number;
  GPT:number;
  Tuition:boolean;
  Predictresult:string = null;
  email;
  predictions = [
    {value: 'Will Quit!', viewValue: 'Will Quit!'},
    {value: 'Will not Quit', viewValue: 'Will not Quit'},
    
  ];
  add(student:Student){
    this.studentService.addStudent(this.email, student.GPT, student.SAT, student.Tuition, student.result)
  }
  getEmail(){
    this.authService.getUser().subscribe(
      res=>{
        this.email = res.email
      }
    )
    return this.email
  }
onSubmit(){}
predict(GPT, SAT, Tuition){
  this.predictService.predict(GPT, SAT, Tuition).subscribe(
      res =>{
        console.log(res);
        if (res>0.5){
          var result = "Will not Quit"}
          else{
            var result = "Will Quit!"
          }
          this.Predictresult = result;
    }
   
  )

}
  constructor( private authService:AuthService, private predictService:PredictService, private studentService:StudentService ) { }

  ngOnInit(): void {
  }

}
