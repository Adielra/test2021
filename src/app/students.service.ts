import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Student } from './interfaces/student';

@Injectable({
  providedIn: 'root'
})

export class StudentService {

  userCollection:AngularFirestoreCollection = this.db.collection('email');
  studentsCollection:AngularFirestoreCollection;

  getStudents(userId): Observable<any[]> {
    this.studentsCollection = this.db.collection(`email/${userId}/students`, 
       ref => ref.limit(10))
    return this.studentsCollection.snapshotChanges();    
  } 
  deleteStudents(userId:string, id:string){
    this.db.doc(`email/${userId}/students/${id}`).delete();
  }
  addStudent(userEmail:string, GPT:number, SAT:number, Tuition:boolean, result:string){
    const student:Student = {GPT:GPT,SAT:SAT, Tuition:Tuition, result:result}
    this.userCollection.doc(userEmail).collection('students').add(student);
  } 
  
  

  constructor(private db: AngularFirestore) { }
}
