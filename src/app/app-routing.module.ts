import { WellcomeComponent } from './wellcome/wellcome.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { StudentsComponent } from './students/students.component';
import { StudentFormComponent } from './student-form/student-form.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent},
  { path: 'wellcome', component: WellcomeComponent},
  {path: 'studentfrom', component:StudentFormComponent},
  {path: 'students', component:StudentsComponent},
  { path: 'signup', component: SignUpComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
