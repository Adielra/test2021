import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class PredictService {
  private url =  "https://s6e6g08pyg.execute-api.us-east-1.amazonaws.com/beta";

  predict(GPT:number, SAT:number, Tuition:boolean){
    let json= {
      'data':{
        "GPT":GPT,
        "SAT":SAT,
        "Tuition":Tuition
      }
    }
    let body = JSON.stringify(json);
    return this.http.post<any>(this.url, body).pipe(
      map(res=>{
        console.log(res);
        let final = res.body;
        console.log(final);
        final = final.replace('[','')
        final = final.replace(']','')
        return final
      })
    )
    
  }

  constructor(private http:HttpClient) { }
}